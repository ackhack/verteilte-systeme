import java.rmi.Naming;

public class Client {
    public static void main(String[] args) throws Exception {

        ServerAgent serverAgent = (ServerAgent) Naming.lookup("rmi://localhost:8080/Agent");
        DemoAgent agent = new DemoAgent(5);
        agent = (DemoAgent) serverAgent.execute(agent);
        System.out.println(agent.getResult());
    }
}