import java.rmi.Naming;
import java.time.LocalTime;

public class DaytimeClient {
    public static void main(String[] args) throws Exception {

        Daytime daytime = (Daytime) Naming.lookup("rmi://localhost:8080/Daytime");
        System.out.println("ClientTime before: "+ LocalTime.now());
        System.out.println(daytime.getEcho());
        System.out.println("ClientTime after: "+LocalTime.now());

    }
}