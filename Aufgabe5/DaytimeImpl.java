import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalTime;

public class DaytimeImpl extends UnicastRemoteObject implements Daytime {

    DaytimeImpl() throws RemoteException {
        super();
    }

    @Override
    public String getEcho() {
        LocalTime time = LocalTime.now();
        System.out.println("Client asked for Time: " + time);
        return "Servertime: " + time;
    }
}