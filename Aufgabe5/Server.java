import java.io.IOException;
import java.rmi.registry.LocateRegistry;

public class Server {
    public static void main(String[] args) throws IOException {
        LocateRegistry.createRegistry(8080).rebind("Agent", new ServerAgentImpl());
    }
}
