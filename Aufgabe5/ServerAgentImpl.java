import java.rmi.*;
import java.rmi.server.*;

public class ServerAgentImpl extends UnicastRemoteObject implements ServerAgent {

    protected ServerAgentImpl() throws RemoteException {
    }

    @Override
    public Agent execute(Agent agent) {
        agent.execute();
        return agent;
    }
}
