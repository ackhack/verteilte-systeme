import java.io.IOException;
import java.rmi.registry.LocateRegistry;

public class DaytimeServer {

    public static void main(String[] args) throws IOException {
        LocateRegistry.createRegistry(8080).rebind("Daytime", new DaytimeImpl());
    }
}