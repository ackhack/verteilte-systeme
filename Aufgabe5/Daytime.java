import java.rmi.Remote;
import java.rmi.RemoteException;

interface Daytime extends Remote {
    String getEcho() throws RemoteException;
}

