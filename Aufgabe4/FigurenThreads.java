public class FigurenThreads {
    public static synchronized void main(String[] args) {

        OwnFigur f = new OwnFigur('A', 1);
        new Leser(f).start();
        Schreiber writer = new Schreiber(f);
        writer.setDaemon(true);
        writer.start();
    }
}
