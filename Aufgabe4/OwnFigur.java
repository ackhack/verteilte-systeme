public class OwnFigur extends Figur {

    OwnFigur(char x, int y) {
        setPosition(x,y);
    }

    @Override
    synchronized void setPosition(char x, int y) {
        this.x = x;
        MachMal.eineZehntelSekundeLangGarNichts();
        this.y = y;
    }

    @Override
    synchronized String getPosition() {
        try {
            Thread.sleep(1000);
        } catch (Exception ignored) {}
        return "X: " + x + " Y: " + y;
    }
}
