// Datei: UniqueId.java
import java.io.*;

class UniqueId {

	private String file;
	private static UniqueId ui;

	private UniqueId(String file) {
		this.file = file;
	}

	static UniqueId getInstance(String file) {
		if (ui == null) {
			ui = new UniqueId(file);
		}
		return ui;
	}

	void init(int id) throws IOException {
		DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
		out.writeInt(id);
		out.close();
	}		

	synchronized int getNext() throws IOException {
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		int oldId = in.readInt();
		in.close();
		
		DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
		int newId = ++oldId;
		out.writeInt(newId);
		out.close();

		return newId;
	}
}