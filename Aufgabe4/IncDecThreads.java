// File  : IncDecThreads.java
// Date  : 9.8.2005 23:15:35

// Diese Klasse definiert einen zahler,
// der von einem Thread um die Größe increment
// weitergeschaltet wird.
// In der Anwendung wird als increment nur +1 bzw. -1 benutzt.
// Das Programm entstand in Kooperation mit
// Herrn Dipl. Inf. (FH) Martin Opel

// Ab JDK 1.5


public class IncDecThreads extends Thread {
    // Maximim für die Iteration
    private static final long MAX = 10000000;
    private static final String sync = "null";
    private static boolean finished = false;
    // Um die folgende Größe weiterschalten
    private long increment;

    // Festlegung des Wertes von increment
    private IncDecThreads(long increment) {
        this.increment = increment;
    }

    // Erzeuge die Threads, Ablauf, Warten auf die Threads
    public static void main(String[] args) {
        IncDecThreads thread1 = new IncDecThreads(+1);
        IncDecThreads thread2 = new IncDecThreads(-1);
        long start = System.currentTimeMillis();
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("zaehler: " + zahler + " nach msec: " + (System.currentTimeMillis() - start));
    }


    // Dies ist der zahler, der von diversen Threads
    // bearbeitet wird.
    private static long zahler = 0;

    private void demoSync() throws InterruptedException {
        boolean firstTime = true;
        synchronized (sync) {
            for (long i = 0; i < MAX; i++) {
                if (!firstTime && !finished) {
                    sync.wait();
                } else {
                    firstTime = false;
                }
                zahler += increment;
                if (i % 100000 == 0) {
                    System.out.println(increment + " " + i);
                }
                sync.notify();
            }
            finished = true;
        }
    }

    private void demo() {
        //Put thread2.start(); behind thread1.join(); speed up to 80ms
        for (long i = 0; i < MAX; i++) {
            zahler += increment;
            if (i % 100000 == 0) {
                System.out.println(increment + " " + i);
            }
        }
    }

    public void run() {
        try {
            demoSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
