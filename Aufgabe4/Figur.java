public abstract class Figur {
  protected char x;
  protected int  y;
  abstract void setPosition(char x, int y);
  abstract String getPosition();
}
