public class Test extends Thread {

    private static final String path = "Aufgabe4/id.dat";
    private int number;
    private UniqueId unique;

    private Test(UniqueId ui, int i) {
        unique = ui;
        number = i;
    }

    public static void main(String[] args) throws Exception {

        UniqueId ui = UniqueId.getInstance(path);
        ui.init(10000);

        UniqueId ui2 = UniqueId.getInstance(path);
        UniqueId ui3 = UniqueId.getInstance(path);

        System.out.println(ui == ui2);
        System.out.println(ui2 == ui3);
        System.out.println(ui == ui3);

        for (int i = 0; i < 5; i++)
            new Test(ui, i).start();
        new Test(ui2, 5).start();
        new Test(ui3, 6).start();
    }

    public void run() {
        for (int x = 0; x < 10; x++) {
            try {
                System.out.println(number + " " + x + " " + unique.getNext());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}