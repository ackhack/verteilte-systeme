
public class Leser extends Thread {

    private final Figur f;

    Leser(Figur f) {
        this.f = f;
    }

    public void run() {
            for (int i = 1; i <= 30; i++) {
                System.out.println(f.getPosition() + " ");
                if (i % 10 == 0)
                    System.out.println();
            }
        }
}
