#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <time.h>

#define WORK_TIME 8
#define WORK_ROT 3
#define N_PH 5
//Working with Scheduler
//Self-Krüppel

time_t t;
key_t sem_key;
int sem_id;
int sem_num;
static int array[N_PH];

union semnum {

    int val;
    struct semid_ds *buf;
    unsigned short *array;
} arg;

void init_sem()
{
    if ((sem_key = ftok("/home", '1')) < 0)
    {
        perror("Error in ftok");
        exit(1);
    }
    if ((sem_id = semget(sem_key, 1, IPC_CREAT | 0666)) < 0)
    {
        perror("Error in semget");
        exit(1);
    }
    if (semctl(sem_id, sem_num, SETVAL, 1) < 0)
    {
        perror("Error in semctl");
        exit(1);
    }
}

void P(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = -1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1) < 0)
    {
        perror("Error in semop P()");
        exit(1);
    }
}

void V(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = 1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1) < 0)
    {
        perror("Error in semop V()");
        exit(1);
    }
}

void essen(int val)
{
    double x = rand();
    double f = (x / RAND_MAX) * WORK_TIME + 1;
    printf("Hunger %d\n", val);
    P(sem_id);
    int wait = 1;
    while (wait != 0)
    {
        wait = Scheduler(val);
        sleep(1);
        //printf("wait p:%d %d\n",val,wait);
    }
    V(sem_id);
    printf("Essen %d for %d sec\n", val, (int)f);
    sleep((int)f);
    Scheduler(-val);
    //printf("Satt %d\n", val);
}

void denken(int val)
{
    double x = rand();
    double f = (x / RAND_MAX) * WORK_TIME + 1;
    printf("Denken %d for %d sec\n", val, (int)f);
    sleep((int)f);
    //printf("Kopf brennt %d \n", val);
}

void doTasks(int val)
{
    int i = 0;
    srand(val);
    while (i < WORK_ROT)
    {
        denken(val);
        essen(val);
        i++;
    }
}

int Scheduler(int number)
{
    // open with 1 - 5
    //printf("Scheduler %d\n",number);

    if (number > 0)
    {
        number--;
        //printf("Scheduler started %d\n", number);
        //start eating
        if (number == 0)
        {
            if (array[N_PH - 1] == 1 || array[0] == 1)
            {
                return 1;
            }
            array[N_PH - 1] = 1;
            array[0] = 1;
            return 0;
        }
        else
        {
            if (array[number] == 1 || array[number - 1] == 1)
            {
                return 1;
            }
            array[number] = 1;
            array[number - 1] = 1;
            return 0;
        }
    }
    else
    {
        number = -number;
        number--;
        //printf("Scheduler finished %d\n", number);
        //stop eating
        if (number == 0)
        {
            array[N_PH - 1] = 0;
            array[0] = 0;
        }
        else
        {
            array[number] = 0;
            array[number - 1] = 0;
        }
    }
}

int main()
{
    srand(time(NULL));
    init_sem();

    for (int p = 0; p < N_PH; p++)
    {
        int PID = fork();

        switch (PID)
        {

        case -1:
            perror("Fork failed\n");
            exit(1);
            break;

        case 0:

            printf("child process: %d with pid(%d)\n", p + 1, getpid());
            doTasks(p + 1);
            printf("Task %d finished\n", p + 1);
            exit(0);
            break;
        }
    }
    printf("Father Process stopped\n");
    return 0;
}