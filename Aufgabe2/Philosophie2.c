#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <time.h>

time_t t;
key_t sem_key;
int sem_id;
//Working with Sem Arrays
//Aufgabe 1

#define WORK_TIME 3
#define WORK_ROT 3
#define N_PH 5

union semnum {

    int val;
    struct semid_ds *buf;
    unsigned short *array;
} arg;

void init_sem()
{
    if ((sem_key = ftok("/home", '1')) < 0)
    {
        perror("Error in ftok");
        exit(1);
    }

    if ((sem_id = semget(sem_key, N_PH, IPC_CREAT | 0666)) < 0)
    {
        perror("Error in semget");
        exit(1);
    }
    for (int i = 0; i < N_PH; i++)
    {
        if (semctl(sem_id, i, SETVAL, 1) < 0)
        {
            perror("Error in semctl");
            exit(1);
        }
    }
}

void P(int sem_num[])
{
    struct sembuf semaphore[2];
    semaphore[0].sem_num = sem_num[0];
    semaphore[0].sem_op = -1;
    semaphore[0].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    semaphore[1].sem_num = sem_num[1];
    semaphore[1].sem_op = -1;
    semaphore[1].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 2) < 0)
    {
        perror("Error in semop P()");
        exit(1);
    }
}

void V(int sem_num[])
{
    struct sembuf semaphore[2];
    semaphore[0].sem_num = sem_num[0];
    semaphore[0].sem_op = 1;
    semaphore[0].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    semaphore[1].sem_num = sem_num[1];
    semaphore[1].sem_op = 1;
    semaphore[1].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 2) < 0)
    {
        perror("Error in semop V()");
        exit(1);
    }
}

void essen(int val)
{
    double x = rand();
    double f = (x / RAND_MAX) * WORK_TIME + 1;
    printf("Hunger %d\n", val);
    val--;
    int array[2];
    switch (val)
    {
    case 0:
        array[0] = 0;
        array[1] = N_PH - 1;
        break;
    default:
        array[0] = val - 1;
        array[1] = val;
    }
    P(array);
    printf("Essen %d for %d sec\n", val + 1, (int)f);
    sleep((int)f);
    V(array);
    printf("Satt %d\n", val+1);
}

void denken(int val)
{
    double x = rand();
    double f = (x / RAND_MAX) * WORK_TIME + 1;
    printf("Denken %d for %d sec\n", val, (int)f);
    sleep((int)f);
    printf("Denken fertig %d \n", val);
}

void doTasks(int val)
{
    int i = 0;
    srand(val);
    while (i < WORK_ROT)
    {
        denken(val);
        essen(val);
        i++;
    }
}

int main()
{
    srand(time(NULL));
    init_sem();

    for (int p = 0; p < N_PH; p++)
    {
        int PID = fork();

        switch (PID)
        {

        case -1:
            perror("Fork failed\n");
            exit(1);
            break;

        case 0:

            printf("child process: %d with pid(%d)\n", p + 1, getpid());
            doTasks(p + 1);
            printf("Task %d finished\n", p + 1);
            exit(0);
            break;
        }
    }
    printf("Father Process stopped\n");
    return 0;
}