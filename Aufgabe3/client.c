#include <rpc/rpc.h>
#include "math.h"

int x;
int y;

int main(int argc, char* argv[]) {

    intpair* pair;

    if (argc < 4) {
        perror("Not enough Arguments\n");
        return 1;
    }

    CLIENT* cl = clnt_create(argv[1],MATHPROG,MATHVERS, "tcp");

    x = atoi(argv[2]);
    y = atoi(argv[3]);

    pair->a = x;
    pair->b = y;

    printf("Result Add: %d\n",*add_1 (pair, cl));
    printf("Result Multi: %d\n",*multiply_1 (pair, cl));
    printf("Result Cube: %d\n",*cube_1 (&x, cl));
    return 0;
}