#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <time.h>

#define mutex 1
#define writer 0
#define reader 2

time_t t;
key_t sem_key;
int sem_id;

//Aufgabe 1

union semnum {

    int val;
    struct semid_ds *buf;
    unsigned short *array;
} arg;

void init_sem()
{
    sem_key = 45;
    if (0)
    {
        perror("Error in ftok");
        exit(1);
    }

    if ((sem_id = semget(sem_key, 3, IPC_CREAT | 0666)) < 0)
    {
        perror("Error in semget");
        exit(1);
    }

    if (semctl(sem_id, mutex, SETVAL, 1) < 0)
    {
        perror("Error in semctl 1");
        exit(1);
    }

    if (semctl(sem_id, writer, SETVAL, 1) < 0)
    {
        perror("Error in semctl 2");
        exit(1);
    }
    if (semctl(sem_id, reader, SETVAL, 0) < 0)
    {
        perror("Error in semctl 3");
        exit(1);
    }
}

void P(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = -1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1) < 0)
    {
        perror("Error in semop P()");
        exit(1);
    }
}

void V(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = 1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1) < 0)
    {
        perror("Error in semop V()");
        exit(1);
    }
}

void Write(int number)
{
    P(writer);
    printf("%d is writing\n", number + 1);
    sleep(1);
    V(writer);
    printf("%d finished writing\n", number + 1);
    sleep(1);
}

void Read(int number)
{
    P(mutex);

    semctl(sem_id, reader, SETVAL, semctl(sem_id, reader, GETVAL) + 1);

    if (1 == semctl(sem_id, reader, GETVAL))
        P(writer);

    V(mutex);

    printf("%d is reading\n", number + 1);
    sleep(1);
    
    P(mutex);

    semctl(sem_id, reader, SETVAL, (semctl(sem_id, reader, GETVAL) - 1));

    if (0 == semctl(sem_id, reader, GETVAL, 1))
        V(writer);

    printf("%d finished reading in crit\n", number + 1);
    sleep(1);
    V(mutex);
}

int main()
{
    srand(time(NULL));
    init_sem();

    for (int p = 0; p < 7; p++)
    {
        int PID = fork();

        switch (PID)
        {

        case -1:
            perror("Fork failed\n");
            exit(1);
            break;

        case 0:

            printf("child process: %d with pid(%d)\n", p + 1, getpid());

            for (int i = 0; i < 3; i++)
            {
                if (p == 0 || p == 5)
                {
                    Write(p);
                }
                else
                {
                    Read(p);
                }            }

            printf("Task %d finished\n", p + 1);
            exit(0);
            break;
        }
    }
    printf("Father Process stopped\n");
    return 0;
}