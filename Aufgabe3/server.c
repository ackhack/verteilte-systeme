#include <rpc/rpc.h>
#include "math.h"

int result;

int * add_1_svc (intpair* pair,struct svc_req* rqstq) {
	result = pair->a + pair->b;
	printf("%d + %d = %d\n",pair->a,pair->b,result);
	return &result;
}

int * multiply_1_svc (intpair* pair,struct svc_req* rqstq) {
	result = pair->a * pair->b;
	printf("%d * %d = %d\n",pair->a,pair->b,result);
	return &result;
}

int * cube_1_svc (int* pair,struct svc_req* rqstq) {
	result = *pair**pair;
	printf("%d ^2 = %d\n",*pair,result);
	return &result;
}