#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{

    for (int p = 0; p < 3; p++)
    {
        int PID = fork();

        switch (PID)
        {

        case -1:
            perror("Fork failed");
            exit(1);

        case 0:

            printf("child process: %d with pid(%d)\n", p, getpid());

            for (int i = 0; i < 3; i++)
            {
                printf("Crit Start (%d)\n", getpid());
                sleep(1);
                printf("Crit End (%d)\n",getpid());
            }

            exit(0);
        }
    }
    printf("Father Process stopped\n");
    return 0;
}