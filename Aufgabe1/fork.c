#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/sem.h>

key_t sem_key;
int sem_id;
int sem_num;

void init_sem()
{

    if ((sem_key = ftok("/home/ackhack", '1')) < 0)
    {
        perror("Error in ftok");
        exit(1);
    }
    if ((sem_id = semget(sem_key, 1, IPC_CREAT | 0666)) < 0)
    {
        perror("Error in semget");
        exit(1);
    }
    if (semctl(sem_id, sem_num, SETVAL, 1) < 0)
    {
        perror("Error in semctl");
        exit(1);
    }
}

void P(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = -1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1))
    {
        perror("Error in semop P()");
        exit(1);
    }
}

void V(int sem_num)
{
    struct sembuf semaphore;
    semaphore.sem_num = sem_num;
    semaphore.sem_op = 1;
    semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
    if (semop(sem_id, &semaphore, 1))
    {
        perror("Error in semop V()");
        exit(1);
    }
}

int main()
{
    init_sem();

    for (int p = 0; p < 3; p++)
    {
        int PID = fork();

        switch (PID)
        {

        case -1:
            perror("Fork failed");
            exit(1);

        case 0:

            printf("child process: %d with pid(%d)\n", p, getpid());

            for (int i = 0; i < 3; i++)
            {
                P(sem_id);
                printf("Crit Start (%d)\n", getpid());
                sleep(1);
                printf("Crit End (%d)\n", getpid());
                V(sem_id);
            }

            exit(0);
        }
    }
    printf("Father Process stopped\n");
    return 0;
}